cms-client-sdk-python
==================

Python SDK for BlueFrame's client sdk, Version 3 (pre-alpha)

Version 3 changes:
  - Existing volar.py has been renamed blueframe.py: all references to volar.py in test files updated with classnames changed as well (from blueframe import BlueFrame)...
  - 'Sites' definition renamed to 'Info' to better match the api naming schema
  - 'Customer' and 'Site' definitions added to aid in testing existing CMS options

Full Documentation can be found [here](http://volarvideo.github.io/cms-client-sdk-python/)

This is a rework of the existing [Python SDK](https://github.com/volarvideo/cms-client-sdk).  Primary purpose of the rework was to eliminate the step of uploading files directly to the blueframe servers - instead, when videos are archived or posters are uploaded, the files are uploaded to our remote storage and enqueued for transcode, relieving a lot of the work our servers have to do to bring content to viewers.

The downside is that the Python sdk now has a new dependancy - the Amazon AWS SDK, otherwise known as boto.  However, installation of the boto module is easy - follow the instructions on [https://aws.amazon.com/sdkforpython/](https://aws.amazon.com/sdkforpython/)
